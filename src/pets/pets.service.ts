import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UsersService } from 'src/users/users.service';
import { Repository } from 'typeorm';
import { CreatePetsDto } from './dto/create-pets.dto';
import { UpdatePetsDto } from './dto/update-pets.dto';
import { Pets } from './entities/pets.entity';

@Injectable()
export class PetsService {
  constructor(
    @InjectRepository(Pets) private petsDB: Repository<Pets>,
    private userService: UsersService,
  ) {}
  async create(createPetsDto: CreatePetsDto): Promise<Pets> {
    const user = await this.userService.findOne(createPetsDto.userId);
    const newPets = await this.petsDB.create(createPetsDto);
    newPets.user = user;
    return await this.petsDB.save(newPets).catch((err) => {
      throw new HttpException(err.sqlMessage, HttpStatus.CONFLICT);
    });
  }

  async findAll(): Promise<Pets[]> {
    return await this.petsDB.find().catch((err) => {
      throw new HttpException(err.sqlMessage, HttpStatus.FOUND);
    });
  }

  async findOne(id: string) {
    return await this.petsDB.findOneOrFail(id).catch((err) => {
      throw new HttpException('pets not found', HttpStatus.FOUND);
    });
  }

  async update(id: string, updatePetsDto: UpdatePetsDto) {
    const pet = await this.findOne(id);
    await this.petsDB.update(id, updatePetsDto);
    return pet;
  }

  async remove(id: string) {
    const pet = await this.findOne(id);
    return await this.petsDB.delete(id).then((data) => pet);
  }
}
