import { User } from 'src/users/entities/user.entity';
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
@Entity()
export class Pets {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column()
  name: string;
  @Column()
  species: string;
  @Column()
  breed: string;
  @Column()
  color: string;
  @Column()
  dateOfBirth: Date;
  @Column()
  weight: number;
  @ManyToOne(() => User, (user) => user.pets, { onDelete: 'CASCADE' })
  user: User;
}
