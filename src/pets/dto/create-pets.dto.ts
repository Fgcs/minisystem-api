export class CreatePetsDto {
  name: string;
  species: string;
  breed: string;
  color: string;
  dateOfBirth: Date;
  weight: number;
  userId: string;
}
