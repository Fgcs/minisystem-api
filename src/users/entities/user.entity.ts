import { Pets } from 'src/pets/entities/pets.entity';
import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column({ unique: true })
  dni: string;
  @OneToMany(() => Pets, (pets) => pets.user, { onDelete: 'CASCADE' })
  pets: Pets[];
}
