export class CreateUserDto {
  firstName: string;
  lastName: string;
  dni: string;
}
