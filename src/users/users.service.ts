import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private userDb: Repository<User>,
  ) {}
  async create(createUserDto: CreateUserDto): Promise<User> {
    const newUser = await this.userDb.create(createUserDto);
    return await this.userDb.save(newUser).catch((err) => {
      throw new HttpException(err.sqlMessage, HttpStatus.CONFLICT);
    });
  }

  async findAll(): Promise<User[]> {
    return await this.userDb.find().catch((err) => {
      throw new HttpException(err.sqlMessage, HttpStatus.FOUND);
    });
  }

  async findOne(id: string): Promise<User> {
    return await this.userDb
      .findOneOrFail(id, { relations: ['pets'] })
      .catch((err) => {
        throw new HttpException('user not found', HttpStatus.FOUND);
      });
  }

  async update(id: string, updateUserDto: UpdateUserDto): Promise<User> {
    const user = await this.findOne(id);
    await this.userDb.update(id, updateUserDto);
    return user;
  }
  async remove(id: string): Promise<User> {
    const user = await this.findOne(id);
    return await this.userDb.delete(id).then((data) => user);
  }
  async foundDni(dni: string): Promise<User> {
    return await this.userDb.findOne({ dni: dni }, { select: ['id', 'dni'] });
  }
}
